package fr.doudou.loveletter;

public enum CardType {

    GUARD(1, "Guard", "Name a non-Guard card and choose another player. If that player has that card, he or she is out of the round"),
    ROYAL_SUBJECT(2, "Royal subject", "Look at another player's hand"),
    GOSSIP(3, "Gossip", "You and another player secretely compare hands. The player with the lower value is out of the round."),
    COMPANION(4, "Companion", "Until your next turn, ignore all effects from other players' cards."),
    HERO(5, "Hero", "Choose any player including yourself to discard his or her hand and draw a new card."),
    WIZARD(6, "Wizard", "Trade hands with another player of your choice."),
    LADY(7, "Lady", "If you have this card and the Wizard or a Hero in your hand, you must discard this card."),
    PRINCESS(8, "Princess", "If you discard this card, your are out of the round");


    private int force = 0;
    private String name = "";
    private String description = "";

    CardType(int force, String name, String description) {
        this.force = force;
        this.name = name;
        this.description = description;
    }

    public int getForce() {
        return force;
    }


    public String getName() {
        return name;
    }


    public String getDescription() {
        return description;
    }

}
