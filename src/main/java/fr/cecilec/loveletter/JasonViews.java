package fr.doudou.loveletter;

public class JasonViews {
    public static class PublicView { };
    public static class InternalOnlyView extends PublicView { };
}
