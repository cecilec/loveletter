package fr.doudou.loveletter;

public class InvalidMoveException extends Exception {

    String message = "This move is void";

    public InvalidMoveException(Game game, Move move) {
        Player myTarget = game.getPlayers()
                .stream()
                .filter(element -> element.getId().equals(move.getTarget()))
                .findFirst()
                .orElse(null);
        String stringTarget = "";
        String stringAccusation = "";
        String stringCard = "";

        if (move.getCard() != null){
            stringCard = move.getCard().getName();
        }

        if (myTarget != null){
            stringTarget = myTarget.getPseudo();
        }

        if (move.getAccusation() != null){
            stringAccusation = move.getAccusation().getName();
        }

        message = "The move " + stringCard + " on " + stringTarget + " with accusation " + stringAccusation + " is void.";
    }

    public String getMessage(){
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
