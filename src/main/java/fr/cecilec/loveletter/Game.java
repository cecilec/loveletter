package fr.doudou.loveletter;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;

import java.util.*;
import java.util.stream.Collectors;

public class Game {
    


    @JsonView(JasonViews.PublicView.class)
    private UUID id;
    @JsonView(JasonViews.PublicView.class)
    private List<Player> players = new ArrayList<Player>();
    @JsonView(JasonViews.InternalOnlyView.class)
    private List<CardType> cards = new ArrayList<CardType>();
    @JsonView(JasonViews.PublicView.class)
    private int currentPlayerPosition = 0;
    @JsonView(JasonViews.PublicView.class)
    private boolean over = false;
    @JsonView(JasonViews.PublicView.class)
    private Player winner;
    @JsonView(JasonViews.PublicView.class)
    private History history = new History();
    @JsonView(JasonViews.PublicView.class)
    public History getHistory() {
        return history;
    }

    public void setHistory(History history) {
        this.history = history;
    }

    public Player getWinner() {
        return winner;
    }

    public void setWinner(Player winner) {
        this.winner = winner;
    }

    public Game(List<String > playerNames){
        this();
        for (String playerName : playerNames){
            Player player = new Player(playerName);
            player.getHand().add(this.draw());
            this.getPlayers().add(player);
        }
    }


    public Game() {
        this.initCards();
    }

    public List<Player> getPlayers() {
        return players;
    }

    public List<CardType> getCards() {
        return cards;
    }

    public void setCards(List<CardType> cards) {
        this.cards = cards;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public boolean isOver() {
        List<Player> playersAlive = getPlayersAlive();
        if (playersAlive.size() == 1) {
            endGame(playersAlive.get(0));
        }
        return over;
    }

    public void setOver(boolean over) {
        this.over = over;
    }

    public int getCurrentPlayerPosition() {
        return currentPlayerPosition;
    }

    public void setCurrentPlayerPosition(int currentPlayerPosition) {
        this.currentPlayerPosition = currentPlayerPosition;
    }

    public void initCards() {
        this.id = UUID.randomUUID();
        CardType[] types = new CardType[]{
                CardType.GUARD, CardType.GUARD, CardType.GUARD, CardType.GUARD, CardType.ROYAL_SUBJECT,
                CardType.GUARD, CardType.ROYAL_SUBJECT, CardType.GOSSIP, CardType.GOSSIP, CardType.COMPANION,
                CardType.COMPANION, CardType.HERO, CardType.HERO, CardType.WIZARD, CardType.LADY,
                CardType.PRINCESS
        };
        cards.addAll(Arrays.asList(types));
        Collections.shuffle(cards);
    }

    public List<Player> getPlayersAlive(){
        return players.stream()
                .filter(element -> element.isAlive())
                .collect(Collectors.toList());
    }

    public void giveHand() {
        nextPlayer();
        if (cards.size() > 1) {
            draw(getCurrentPlayer());
        } else {
            Player winner = compareFinaleHand();
            endGame(winner);
        }
    }

    public void endGame(Player winner) {
        this.setWinner(winner);
        this.setOver(true);
    }


    public Player compareFinaleHand() {
        List<Player> playersAlive = getPlayersAlive();
        Player bestPlayer = players.get(0);
        int bestForce = 0;
        for (Player player : playersAlive) {
            int forceCarte = player.getForce();
            if (forceCarte > bestForce) {
                bestPlayer = player;
                bestForce = forceCarte;
            }
        }
        return bestPlayer;
    }

    public void nextPlayer() {
        int nbPlayers = players.size();
        int i = 1;
        boolean anAlivePlayerFound = false;
        while (!anAlivePlayerFound && i < nbPlayers) {
            int newPosition = (currentPlayerPosition + i) % nbPlayers;
            Player newCurrentPlayer = players.get(newPosition);
            anAlivePlayerFound = newCurrentPlayer.isAlive();
            if (anAlivePlayerFound) {
                currentPlayerPosition = newPosition;
            }
            i++;
        }
    }

    @JsonIgnore
    public Player getCurrentPlayer() {
        return players.get(currentPlayerPosition);
    }

    public CardType draw() {
        return cards.remove(0);
    }

    public CardType draw(Player player) {
        CardType card = draw();
        player.getHand().add(card);
        return card;
    }

    public String play(Move move) throws InvalidMoveException {
        String cardInfo = "";
        Player currentPlayer = getCurrentPlayer();
        CardType cardToPlay = move.getCard();

        Player target;

        CardType accusation = move.getAccusation();
        switch (cardToPlay) {
            case HERO:
                target = findTarget(move);
                if (target == null) {
                    throw new InvalidMoveException(this, move);
                }
                if (target.equals(currentPlayer)){
                    currentPlayer.getHand().add(CardType.HERO);

                }
                target.getDiscard().addAll(target.getHand());
                target.getHand().clear();
                draw(target);
                if (target.getDiscard().contains(CardType.PRINCESS)) {
                    target.setAlive(false);
                }
                break;
            case GUARD:
                target = findTarget(move);
                if (target == null || accusation == null ) {
                    throw new InvalidMoveException(this, move);
                }
                if (target.equals(currentPlayer)) {
                    if (!checkOnlyTargetable()){
                        throw new InvalidMoveException(this, move);
                    }
                }
                else if (target.getHand().get(0) == accusation) {
                    target.setAlive(false);
                }
                break;
            case ROYAL_SUBJECT:
                target = findTarget(move);
                if (target == null) {
                    throw new InvalidMoveException(this, move);
                }
                if (target.equals(currentPlayer)) {
                    if (!checkOnlyTargetable()){
                        throw new InvalidMoveException(this, move);
                    }
                }
                else {
                    cardInfo = target.getHand().get(0).getName();
                }
                break;
            case GOSSIP:
                target = findTarget(move);
                if (target == null) {
                    throw new InvalidMoveException(this, move);
                }
                if (target.equals(currentPlayer)) {
                    if (!checkOnlyTargetable()){
                        throw new InvalidMoveException(this, move);
                    }
                }
                else {
                    currentPlayer.getHand().remove(CardType.GOSSIP);
                    Player looser = compareHands(currentPlayer, target);
                    if (looser != null) {
                        looser.setAlive(false);
                    }
                    currentPlayer.getHand().add(CardType.GOSSIP);
                }
                break;
            case COMPANION:
                currentPlayer.setUntargetable(true);
                break;
            case WIZARD:
                target = findTarget(move);
                if (target == null) {
                    throw new InvalidMoveException(this, move);
                }
                if (target.equals(currentPlayer)) {
                    if (!checkOnlyTargetable()){
                        throw new InvalidMoveException(this, move);
                    }
                }
                else {
                    CardType targetCard = target.getHand().remove(0);
                    CardType playerCard = currentPlayer.getHand().stream()
                            .filter(element -> element != cardToPlay)
                            .findFirst()
                            .orElse(cardToPlay);
                    currentPlayer.getHand().remove(playerCard);
                    currentPlayer.getHand().add(targetCard);
                    target.getHand().add(playerCard);
                }
                break;
            case LADY:
                break;
            case PRINCESS:
                currentPlayer.setAlive(false);
                break;
        }
        return cardInfo;
    }

    private boolean checkOnlyTargetable() {
        Player currentPlayer = getCurrentPlayer();
        Player possibleTarget = getPlayers()
                .stream()
                .filter(element -> element.isAlive())
                .filter(element -> !element.equals(currentPlayer))
                .filter(element -> !element.isUntargetable())
                .findFirst()
                .orElse(null);
        return (possibleTarget == null);
    }

    private Player compareHands(Player currentPlayer, Player target) {
        Player looser = new Player();
        int currentPlayerForce = currentPlayer.getForce();
        int targetForce = target.getForce();
        if (targetForce > currentPlayerForce) {
            looser = currentPlayer;
        } else if (currentPlayerForce > targetForce) {
            looser = target;
        }
        return looser;
    }

    private Player findTarget(Move move) {
        Player target = getPlayers()
                .stream()
                .filter(element -> element.getId().equals(move.getTarget()))
                .filter(element -> element.isAlive())
                .filter(element -> !element.isUntargetable())
                .findFirst()
                .orElse(null);
        return target;
    }


    public Player getPlayerByToken(UUID token) {
        Player player = getPlayers()
                .stream()
                .filter(element -> element.getToken().equals(token))
                .findFirst()
                .orElse(null);
        return player;
    }
}
