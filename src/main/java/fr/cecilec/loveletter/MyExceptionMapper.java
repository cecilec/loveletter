package fr.doudou.loveletter;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class MyExceptionMapper implements ExceptionMapper<Exception> {
    public Response toResponse(Exception e) {
        return Response.status(500).entity(new Error(e))
                .type(MediaType.APPLICATION_JSON).build();
    }
}
