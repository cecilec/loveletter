package fr.doudou.loveletter;

public class Error {
    public String message = "Error";

    public Error() {

    }

    public Error(Exception e){
        e.printStackTrace();
        message = e.getMessage();
    }
}
