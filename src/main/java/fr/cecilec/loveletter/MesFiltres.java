package fr.doudou.loveletter;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.BeanPropertyWriter;
import com.fasterxml.jackson.databind.ser.PropertyFilter;
import com.fasterxml.jackson.databind.ser.PropertyWriter;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;

public class MesFiltres {

    public PropertyFilter theFilter = new SimpleBeanPropertyFilter() {
        @Override
        public void serializeAsField
        (Object reponse, JsonGenerator jgen, SerializerProvider provider, PropertyWriter writer)
     throws Exception {
            if (include(writer)) {
                if (!writer.getName().equals("intValue")) {
                    writer.serializeAsField(reponse, jgen, provider);
                    return;
                }
                boolean finie = ((Response) reponse).getGame().isOver();
                if (!finie) {
                    writer.serializeAsField(reponse, jgen, provider);
                }
            } else if (!jgen.canOmitFields()) { // since 2.3
                writer.serializeAsOmittedField(reponse, jgen, provider);
            }
        }
        @Override
        protected boolean include(BeanPropertyWriter writer) {
            return true;
        }
        @Override
        protected boolean include(PropertyWriter writer) {
            return true;
        }
    };

    //http://www.baeldung.com/jackson-serialize-field-custom-criteria
}
