package fr.doudou.loveletter;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Player {
    private String pseudo;
    private List<CardType> hand = new ArrayList<CardType>();
    private List<CardType> discard = new ArrayList<CardType>();
    private UUID token;
    private UUID id;
    private boolean alive = true;
    private boolean untargetable = false;

    public UUID getId() {
        return id;
    }

    public boolean isUntargetable() {
        return untargetable;
    }

    public void setUntargetable(boolean untargetable) {
        this.untargetable = untargetable;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Player(){
        this.token = UUID.randomUUID();
        this.id = UUID.randomUUID();
    }

    public Player(String pseudo){
        this();
        this.pseudo = pseudo;
    }
    public boolean isAlive() {
        return alive;
    }

    public void setAlive(boolean alive) {
        this.alive = alive;
        if (!alive){
            discard.addAll(hand);
            hand.clear();
        }
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public List<CardType> getHand() {
        return hand;
    }

    public void setHand(List<CardType> hand) {
        this.hand = hand;
    }

    public List<CardType> getDiscard() {
        return discard;
    }

    public void setDiscard(List<CardType> discard) {
        this.discard = discard;
    }

    public UUID getToken() {
        return token;
    }

    public void setToken(UUID token) {
        this.token = token;
    }

    @Override
    public boolean equals(Object obj) {
        if ((null == obj) || (obj instanceof Player))
            return false;
        Player myPlayer = (Player) obj;
        return (myPlayer.getId().equals(this.getId()));
    }

    public int getForce(){
        return this.getHand().get(0).getForce();
    }
}
