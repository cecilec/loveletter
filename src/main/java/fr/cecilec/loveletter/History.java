package fr.doudou.loveletter;

import java.util.ArrayList;
import java.util.List;

public class History {
    List<Move> moves = new ArrayList<Move>();

    public List<Move> getMoves() {
        return moves;
    }

    public void setMoves(List<Move> moves) {
        this.moves = moves;
    }
}
