package fr.doudou.loveletter;

import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.jaxrs.annotation.JacksonFeatures;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.UUID;

@Path("/")
public class Resource {

    /**
     * Resource serves the webservice with infos on the game
     * and the way to update that info when doing a move
     */

    /**
     * Creates a new LoveLetter Game
     * @param playerNames the names of whom will play
     * @return the public information about the game that has been created as a JSON
     */
    @POST
    @Path("create")
    @Consumes(MediaType.APPLICATION_JSON)
    @JacksonFeatures(serializationEnable = {SerializationFeature.INDENT_OUTPUT})
    @Produces(MediaType.APPLICATION_JSON)
    @JsonView(JasonViews.PublicView.class)
    public Response create(List<String > playerNames){
        Game game = new Game(playerNames);
        game.getPlayers().get(0).getHand().add(game.draw());
        State.PARTIES.put(game.getId(), game);
        Response response = new Response(game, "");
        return (response);
    }


    /**
     * Gives the private info about an ongoing game
     * @param token the token of the player which gives access to the private info
     * @param idGame the id of the game on which infos are asked for
     * @return the private info of the game as a JSON
     */
    @GET
    @Path("partie/{idParty}")
    @JsonView(JasonViews.PublicView.class)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getGameStatus(@HeaderParam("Authorization") UUID token, @PathParam("idParty") UUID idGame){
        Game game = State.PARTIES.get(idGame);
        Player player = game.getPlayerByToken(token);
        if (player == null){
            throw new AccessException();
        }
        Response response = new Response(game, "");
        return (response);
    }

    /**
     * Plays a turn in the game
     * @param move the move the player wants to do
     * @param token the token of the current player
     * @param idGame the id of the game
     * @return the new infos on the game after the move as a JSON
     * @throws InvalidMoveException if its not the player turn or if he plays an illegal move (like the lady with a hero)
     */
    @POST
    @Path("play/{idParty}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @JsonView(JasonViews.PublicView.class)
    @JacksonFeatures(serializationEnable = {SerializationFeature.INDENT_OUTPUT})
    public Response play(Move move, @HeaderParam("Authorization") UUID token, @PathParam("idParty") UUID idGame) throws InvalidMoveException {
        Game game = State.PARTIES.get(idGame);
        Response response = new Response(game, "");
        if (isCurrentPlayer(game, token)){
            Player currentPlayer = game.getCurrentPlayer();
            CardType cardToPlay = move.getCard();
            if (ladyProblem(game, move)){
                throw new InvalidMoveException(game, move);
            }
                if (currentPlayer.getHand().contains(cardToPlay)) {
                    String cardInfo = game.play(move);
                    response.setCardInfo(cardInfo);
                    currentPlayer.getHand().remove(cardToPlay);
                    currentPlayer.getDiscard().add(cardToPlay);
                    game.giveHand();
                    game.getCurrentPlayer().setUntargetable(false);
                    game.getHistory().getMoves().add(move);
                }
                else {
                    throw new InvalidMoveException(game, move);
            }
        }
        else {
            throw new AccessException();
        }
        return (response);
    }

    /**
     * Checks if the move is illegal because the lady as been played with a hero or a wizard
     * @param game the current game
     * @param move the move to check
     * @return true if the move is illegal because of the lady, false otherwise
     */
    private boolean ladyProblem(Game game, Move move) {
        Player currentPlayer = game.getCurrentPlayer();
        if ((move.getCard() != CardType.LADY) && currentPlayer.getHand().contains(CardType.LADY) &&
                (currentPlayer.getHand().contains(CardType.HERO) || currentPlayer.getHand().contains(CardType.WIZARD))){
            return true;
        }
        return false;
    }

    /**
     * Checks if it is the given player turn
     * @param game the current game
     * @param token the token of the player to test
     * @return true if it is the given player turn
     */
    public boolean isCurrentPlayer(Game game, UUID token){
        Player currentPlayer = game.getCurrentPlayer();
        return currentPlayer.getToken().equals(token);
    }

}
