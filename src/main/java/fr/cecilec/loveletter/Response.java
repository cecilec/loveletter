package fr.doudou.loveletter;

import com.fasterxml.jackson.annotation.JsonView;

//@JsonFilter("myFilter")
public class Response {
    @JsonView(JasonViews.PublicView.class)
    private Game game;
    @JsonView(JasonViews.PublicView.class)
    private String cardInfo;

    public Response(){

    }

    public Response(Game game, String cardInfo){
        this.cardInfo = cardInfo;
        this.game = game;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public String getCardInfo() {
        return cardInfo;
    }

    public void setCardInfo(String cardInfo) {
        this.cardInfo = cardInfo;
    }

}
