package fr.doudou.loveletter;

import java.util.UUID;

public class Move {
    private CardType card;
    private CardType accusation;
    private UUID target;

    public CardType getAccusation() {
        return accusation;
    }

    public void setAccusation(CardType accusation) {
        this.accusation = accusation;
    }

    public UUID getTarget() {
        return target;
    }

    public void setTarget(UUID target) {
        this.target = target;
    }

    public CardType getCard() {
        return card;
    }

    public void setCard(CardType card) {
        this.card = card;
    }
}
